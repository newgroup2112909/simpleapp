# SimpleApp



## Add your files
- push an existing Git repository with the following command:

```
cd existing_repo
git add .
git status
git commit -m 'new files added'
git remote add origin https://gitlab.com/newgroup2112909/simpleapp.git
git push -uf origin main
```

## Jenkinsfile
- file containing stages for checkout, build, test and deploy
- Chechout: checkout the gitlab repo using credential 
- Build: build the SimpleApp
- Test: test the SimpleApp
- DEploy: deploy the SimpleApp

## GitLab CI Configuration
- generate Jenkins API token
- install Gitlab plugin in Available plugins
- go to your repository -> settings -> CI/CD -> Variables
```
Add a variable named JENKINS_URL with the value being the URL of your Jenkins server.
Add a variable named JENKINS_TOKEN with the value being the API token you generated
```
- GitLab repository, go to Settings > Integrations
- add a new webhook
- Set the URL to http://YOUR_JENKINS_SERVER/project/YOUR_JENKINS_JOB_NAME/build?token=YOUR_JENKINS_TOKEN 
- Choose the events that should trigger the webhook.
- Save the webhook.

## .gitlab-ci.yml
- write a code to trigger the Jenkins pipeline on each commit to the repository
- save the file
